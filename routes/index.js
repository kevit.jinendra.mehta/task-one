/**
 * Router Configuration Files
 */

/**
 * System and 3rd party libs
 */
const express = require('express');
const bodyParser = require('body-parser');
const _ = require('lodash');
const fs = require('fs');
const path = require('path');
const hbs = require('hbs');
const url = require('url');
const router = express.Router();

const User = require('./../models/user.model');
const mongoose = require('mongoose');
const authenticate = require('./middleware').authenticate;
/**
 * MIME Types
 */
let mime = {
    html: 'text/html',
    txt: 'text/plain',
    css: 'text/css',
    gif: 'image/gif',
    jpg: 'image/jpeg',
    png: 'image/png',
    svg: 'image/svg+xml',
    js: 'application/javascript'
};

/**
 * Router Definitions
 */
router.post('/signup', (req, res) => {
    let user = new User(_.pick(req.body, ['name', 'email', 'password', 'phone']));

    user.save().then(() => {
        res.status('200').send(user);

    }).catch((e) => res.status(401).send(e))
});

router.post('/login', (req, res) => {
    let body = _.pick(req.body, ['email', 'password']);

    User.findByCredentials(body.email, body.password).then((user) => {
        return user.generateAuthToken().then((token) => {
            res.header('x-auth', token).send(user);
        });
    }).catch((e) => {
        res.status(401).send();
    });
});

router.post('/postphoto', authenticate, (req, res) => {
    let imagepath = req.body.imgpath;
    let photo = fs.readFileSync(imagepath);
    let basename = path.basename(imagepath);

    if (!photo) {
        return res.status(401).send('Can\'t get photo');
    }

    req.user.photo = { image: photo, name: basename };

    req.user.save().then(() => {
        res.status(200).send();
    }).catch(e => res.status(401).send());

});

router.get('/:email', (req, res) => {
    let token = req.header('x-auth');

    User.findByEmail(req.params.email).then((user) => {

        return user;
    }).then((userByEmail) => {

        User.findByToken(token).then((userByToken) => {

            if (userByToken && userByToken._id.toHexString() == userByEmail._id.toHexString()) {

                res.render('privateprofile.hbs', {
                    name: userByToken.name,
                    path: 'http://' + req.headers.host + url.parse(req.url).pathname + '/photo',
                    email: userByToken.email,
                    phone: userByToken.phone,
                    password: userByToken.password
                });
            } else {
                res.render('publicprofile.hbs', {
                    name: userByEmail.name,
                    path: 'http://' + req.headers.host + url.parse(req.url).pathname + '/photo'
                });
            }
        }, (e) => {
            res.render('publicprofile.hbs', {
                name: userByEmail.name,
                path: 'http://' + req.headers.host + url.parse(req.url).pathname + '/photo'
            });
            }
        );
    }).catch((e) => {
        res.status(400).send();
    });


});

router.get('/:email/photo', (req, res) => {
    User.findByEmail(req.params.email).then((user) => {
        if (user.photo.name) {
            let extension = path.extname(user.photo.name).slice(1);
            res.type(mime[extension]);
            res.send(user.photo.image);
        } else {
            res.send();
        }
    }, (e) => {
        res.sendStatus(400);
    });
});

router.patch('/update', authenticate, (req, res) => {
    let { name, email, phone, password, imgpath } = req.body;

    if (name) {
        req.user.name = name;
    }

    if (email) {
        req.user.email = email;
    }

    if (phone) {
        req.user.phone = phone;
    }

    if (password) {
        req.user.password = password;
    }

    if (imgpath) {
        let photo = fs.readFileSync(imgpath);
        let basename = path.basename(imgpath);

        if (!photo) {
            return res.status(400).send();
        }
        req.user.photo = { image: photo, name: basename };
    }

    req.user.save().then(() => {
        res.status(200).send();
    }).catch(e => res.status(400).send());
});

router.delete('/logout', authenticate, (req, res) => {
    req.user.removeToken(req.token).then(() => {
        res.status(200).send();
    }, () => {
        res.status(401).send();
    });
});

/**
 * Export Router
 */
module.exports = router;
