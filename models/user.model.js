/**
 * Model Definition File
 */

/**
 * System and 3rd Party libs
 */
const _validator = require('validator');
const jwt = require('jsonwebtoken');
const _ = require('lodash');
const bcrypt = require('bcryptjs');

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

/**
 * Schema Definition
 */

let userSchema = mongoose.Schema({
    name: {
        type: Schema.Types.String,
        trim: true,
        minlength: 1
    },
    email: {
        type: Schema.Types.String,
        trim: true,
        required: true,
        minlength: 1,
        unique: true,
        validate: {
            validator: _validator.isEmail,
            message: '{VALUE} is not a valid email'
        }
    },

    password: {
        type: Schema.Types.String,
        minlength: 6,
        required: true,
    },

    phone: {
        type: Schema.Types.String,
        trim: true,
        required: true,
        unique: true,
        validate: {
            validator: function (v) {
                return _validator.isNumeric(v) && v.length == 10;
            },
            message: '{VALUE} is not a valid phone number'
        }
    },

    photo: {
        image: {
            type: Schema.Types.Buffer,
        },
        name: {
            type: Schema.Types.String,
        }
    },

    tokens: [{
        access: {
            type: Schema.Types.String,
            required: true
        },
        token: {
            type: Schema.Types.String,
            required: true
        }
    }]
});

/**
 * JSON Response 
 */
userSchema.methods.toJSON = function () {
    var userObject = this.toObject();
    return _.pick(userObject, ['name', 'email', 'phone']);
};

/**
 * Static Methods
 */
userSchema.statics.findByToken = function (token) {
    let User = this;
    let decoded;

    try {
        decoded = jwt.verify(token, "$ecret$Alt#@");
    } catch (e) {
        return Promise.reject();
    }

    return User.findOne({
        _id: decoded._id,
        'tokens.token': token,
        'tokens.access': 'auth'
    });
};

userSchema.statics.findByCredentials = function (email, password) {
    let User = this;

    return User.findOne({ email }).then((doc) => {
        if (!doc) {

            return Promise.reject();
        }

        return new Promise((resolve, reject) => {
            bcrypt.compare(password, doc.password, (err, res) => {
                if (res) {
                    resolve(doc);
                } else {
                    reject();
                }
            });
        });
    }, (e) => reject());
};

userSchema.statics.findByEmail = function (email) {
    let User = this;

    return User.findOne({ email }).then((doc) => {
        if (!doc) {
            return Promise.reject();
        } else {
            return Promise.resolve(doc);
        }
    });
};

/**
* Instance Methods
*/
userSchema.methods.generateAuthToken = function () {
    let user = this;
    let access = 'auth';

    let token = jwt.sign({ _id: user._id.toHexString(), access }, "$ecret$Alt#@").toString();

    user.tokens.push({ access, token });

    return user.save().then(() => {
        return token;
    });
};

userSchema.methods.removeToken = function (token) {
    let user = this;

    return user.updateOne({
        $pull: {
            tokens: { token }
        }
    });
};

/**
 * Middleware
 */
userSchema.pre('save', function (next) {
    let user = this;

    if (user.isModified('password')) {
        bcrypt.genSalt(10, (err, salt) => {
            bcrypt.hash(user.password, salt, (err, hash) => {
                user.password = hash;
                next();
            });
        });
    } else {
        next();
    }
});

/**
 * Export Schema
 */
module.exports = mongoose.model('user', userSchema);